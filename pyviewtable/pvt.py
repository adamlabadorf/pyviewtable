#!/usr/bin/env python
'''
Usage: pvt [options] [--comment=COM]... (<tabular_fn> |-)

Python View Table - a simple utility for looking at column-delimited data on
the command line. Uses the terminaltables.

May also be used with redirection or an output file to pretty-print tables.

NB: the entire file is read into memory. May not be suitable for very large
files.

Upon invocation, commands are:
  q, ESC - quit
  any other key - page down one screen height


Options:
  -h --help
  -d DELIM --delimiter=DELIM    file delimiter, sniffed if omitted [default: ,]
  -f --freeze                   freeze first non-comment row and show on every
                                page
  --cell-limit=LIM              only print out this many characters max per
                                field
  --comment=COM                 ignore lines that start with this character, may
                                be specified multiple times [default: #]
  --strip-comments              do not display comment lines
  --noheader                    file should not be considered to have a header
  -o FILE --output=FILE         write the output to file instead of paginated
                                stdout
'''

import csv
from docopt import docopt
import io
import fabulous
import os
import pager
import sys
import terminaltables

def main(argv=None) :

  opts = docopt(__doc__,argv=argv)

  tab_fn = opts['<tabular_fn>']

  if tab_fn == '-' : # read from stdin
    content = sys.stdin.read()
  else :
    with open(tab_fn,'rt') as tab_f :
      content = tab_f.read()

  reader = csv.reader(content.split('\n'),delimiter=opts['--delimiter'])

  def comment_line(st) :
    return any(st.startswith(_) for _ in opts['--comment'])

  lines = []
  comment_lines = []
  for i,r in enumerate(reader) :
    if not r or comment_line(r[0]) :
      comment_lines.append((i,r))
    else :
      lines.append(r)
  # the last line may be blank
  if not lines[-1] :
    lines = lines[:-1]
  # the last line may be blank
  if not comment_lines[-1] :
    comment_lines = comment_lines[:-1]


  if opts['--noheader'] :
    lines.insert(0,['x']*len(lines[0]))

  if opts['--cell-limit'] :
    try :
      limit = int(opts['--cell-limit'])
      assert limit > 0
    except (ValueError, AssertionError) :
      print('--cell-limit must be a positive integer if provided, aborting'
        ,file=sys.stderr
      )
      sys.exit(1)

    for i in range(1,len(lines)) : # don't limit the header
      for j in range(len(lines[i])) :
        if len(lines[i][j]) > limit :
          lines[i][j] = lines[i][j][:limit]+'\u2026'

  tab = terminaltables.AsciiTable(lines)

  table = tab.table.split('\n')

  # get the number of header lines, useful for --noheader and --freeze
  for num_header_lines, l in enumerate(table[1:]) :
    if l.startswith('+') : break
  num_header_lines += 1
  table_header = table[:num_header_lines+1]

  if opts['--noheader'] :
    table = table[num_header_lines:]

  # put the comments back in
  if not opts['--strip-comments'] :
    for i,(orig_i,c) in enumerate(comment_lines) :
      table.insert(orig_i,opts['--delimiter'].join(c))

  # put a header on each page
  if opts['--freeze'] :
    if opts['--noheader'] :
      print('--freeze and --noheader are not compatible together, aborting'
        ,file=sys.stderr
      )
      sys.exit(2)

    page_size = pager.getheight()-num_header_lines+1
    # insert the header rows into the table at an interval of term_height
    for i in range(page_size,len(table),page_size) :
      for th in table_header[::-1] :
        table.insert(i,th)

  # don't use pager if redirecting to stdout or file
  if opts['--output'] or not sys.stdout.isatty() :
    table_str = '\n'.join(table)
    if opts['--output'] :
      with open(opts['--output'],'wt') as f :
        f.write(table_str)
    else :
      print(table_str,file=sys.stdout)
  else :
    # this bug
    # https://bitbucket.org/techtonik/python-pager/issues/7/33-piping-input-doesnt-work-for-linux
    old_stdin = sys.stdin
    sys.stdin = open(os.ctermid(),'r')
    pager.page(iter(table))
    sys.stdin = old_stdin

if __name__ == '__main__' :
  main()

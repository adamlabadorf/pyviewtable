This script pretty prints tabular files to the terminal. The software
depends on python packages:

  - docopt
  - terminaltables
  - fabulous
  - pager

It has only been poorly tested with python 3.5.

Output from `pvt --help`:

```
Usage: pvt [options] [--comment=COM]... (<tabular_fn> |-)

Python View Table - a simple utility for looking at column-delimited data on
the command line. Uses the terminaltables.

May also be used with redirection or an output file to pretty-print tables.

NB: the entire file is read into memory. May not be suitable for very large
files.

Upon invocation, commands are:
  q, ESC - quit
  any other key - page down one screen height


Options:
  -h --help
  -d DELIM --delimiter=DELIM    file delimiter, sniffed if omitted [default: ,]
  -f --freeze                   freeze first non-comment row and show on every
                                page
  --cell-limit=LIM              only print out this many characters max per
                                field
  --comment=COM                 ignore lines that start with this character, may
                                be specified multiple times [default: #]
  --strip-comments              do not display comment lines
  --noheader                    file should not be considered to have a header
  -o FILE --output=FILE         write the output to file instead of paginated
                                stdout
```
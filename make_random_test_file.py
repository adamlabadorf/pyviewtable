import csv
import random
import string
import sys

if __name__ == '__main__' :
  rows, cols = 100, 10
  datum_len = 10


  data = []
  for i in range(rows) :
    data.append([])
    for j in range(cols) :
      data[-1].append(''.join(random.sample(string.ascii_letters,datum_len)))

  writer = csv.writer(sys.stdout)
  writer.writerows(data)

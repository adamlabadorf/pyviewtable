#!/usr/bin/env python

from setuptools import setup, find_packages

setup(name='pyviewtable',
      version='0.1'
      ,description='Command line tool for viewing tabular files'
      ,author='Adam Labadorf'
      ,author_email='labadorf@bu.edu'
      ,install_requires=[
        'docopt'
        ,'fabulous'
        ,'terminaltables'
        ,'pager'
      ]
      ,packages=find_packages()
      ,entry_points={
        'console_scripts': [
          'pvt=pyviewtable.pvt:main'
        ]
      }
     )
